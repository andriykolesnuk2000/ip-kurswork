<?php


class Route
{
static function start()
{
    session_start();
    // контролер та дія за замовчуванням
    $controller_name = 'Main';
    $action_name = 'index';

    $routes = explode('/', $_SERVER['REQUEST_URI']);
    $param = null;
    $lastPart = array_pop($routes);

    $lastPart = explode('?', $lastPart);
    $routes[] = $lastPart[0];

    // отримуємо ім’я контролера
    if ( !empty($routes[1]) )
    {
    $controller_name = $routes[1];
    }

    // отримуємо ім’я екшена
    if ( !empty($routes[2]) )
    {
    $action_name = $routes[2];
    }

    if ( !empty($routes[3]) )
    {
        $param = $routes[3];
    }

    if ( !empty($routes[4]) )
    {
        $param .= '/' . $routes[4];
    }
    //echo $controller_name . ' ' . $action_name . '<br>';

    // додаємо префікси
    $model_name = 'Model_'.$controller_name;
    $controller_name = 'Controller_'.$controller_name;
    $action_name = 'action_'.$action_name;

    //echo $model_name . ' ' . $controller_name . ' ' . $action_name . '<br>';
    // підчеплюємо файл з класом моделі (файлу моделі може і не бути)

    $model_file = strtolower($model_name).'.php';
    $model_path = "application/models/".$model_file;

    //echo $model_file . ' ' . $model_path . '<br>';

    if(file_exists($model_path))
    {
    include "application/models/".$model_file;
    }

    // підчеплюємо файл з класом  контроллера
    $controller_file = strtolower($controller_name).'.php';
    $controller_path = "application/controllers/".$controller_file;
    if(file_exists($controller_path))
    {
    include "application/controllers/".$controller_file;
    }
    else
    {
    /*
    Тут вірно записати виключення, але ми одразу робимо переключення на сторінку 404
    */
    //    include "application/controllers/controller_404.php";
    //    $controller_file = "controller_404.php";
    //    $controller_name = "controller_404";
    //    $action_name = 'action_index';
        Route::ErrorPage404();
    }

    // створюємо контролер
    $controller = new $controller_name;
    $action = $action_name;

    if(method_exists($controller, $action))
    {
    // вызываем действие контроллера
        if($param == null)
            $controller->$action();
        else
            $controller->$action($param);
    }
    else
    {
        // тут потрібно записати виключення
        Route::ErrorPage404();
    }

}

static function ErrorPage404()
{
    include "application/controllers/controller_404.php";

    $controller_name = "controller_404";
    $action_name = 'action_index';

    $controller = new $controller_name;
    $action = $action_name;

    if(method_exists($controller, $action))
    {
// вызываем действие контроллера
        $controller->$action();
    }
    exit();
//$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
//header('HTTP/1.1 404 Not Found');
//header("Status: 404 Not Found");
//header('Location:'.$host.'404');
}
}
