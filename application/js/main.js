function setValue(className) {
    const newValue = event.target.value;
    const el = document.querySelector('.' + className);
    if(el.min <= +newValue && el.max >= +newValue)
        el.value = +newValue;
}

function marcAsActive(className) {
    let element = document.querySelector('.' + className);

    if(element)
        element.classList.toggle('active')
}

function marcActiveProductPage(number) {
    let element = document.querySelector('.page-' + number);

    if(element)
        element.classList.add('active-product-page')
}

function printError(className, text) {
    let element = document.querySelector('.' + className);

    if(element)
        element.innerHTML = text;
}

function alertMessage(text){
    const messageContainer = document.createElement('div');
    messageContainer.style.position = 'fixed';
    messageContainer.style.top = '20px';
    messageContainer.style.left = 'calc((100% - 200px)/2)';
    messageContainer.style.textAlign = 'center';
    messageContainer.style.background = 'green';
    messageContainer.style.width = '200px';
    messageContainer.style.borderRadius = '5px';
    messageContainer.style.color = 'white';



    messageContainer.innerText = text;

    document.body.append(messageContainer);

    setTimeout(() => {messageContainer.remove()}, 3000);
}