function getCount(chart) {
    let count = 0;

    chart.map((el) => {if(el) count++})
    return count;
}

function showCountInChart() {
    //deleteCookie('chart');
    let chart = [];
    const countContainer = document.querySelector('.count-in-chart');


    if(getCookie('chart')){
        chart = JSON.parse(getCookie('chart'))
        const count = getCount(chart);

        countContainer.innerText = count;
        console.log(count);
        return;
    }

    countContainer.innerText = '0';
}

function addToChart(id, name, photo, cost, inStock) {
    let chart = [];

    if(getCookie('chart')){
        chart = JSON.parse(getCookie('chart'))
        if(chart[id]){
            if(inStock <= chart[id].count) return;
           chart[id].count = chart[id].count + 1;
           setCookie('chart', JSON.stringify(chart));
            return;
        }
    }



    chart[id] = {
        id: id,
        name: name,
        photo: photo,
        cost: cost,
        inStock: inStock,
        count: 1
    }



    setCookie('chart', JSON.stringify(chart));
    // $.ajax({
    //     url:"function.php",
    //     data:"id=2",
    //     success:function(data){
    //         // data - это переменная с ответом от сервера
    //         // В ней хранится все, что было выведено echo'м или die'м в PHP
    //         // да и вообще любой вывод из этого файла
    //     }
    // });
    showCountInChart();
}

function createChartElement(el) {
    let div = document.createElement('div');
    div.classList.add('product-'+el.id);
    div.innerHTML = el.name + ' - ' + el.photo + ' - ' + el.count + ' <button onclick="deleteFromChart('+ el.id +')">Видалити</button>' + el.cost;

    div.innerHTML = '<table>' +
                        '<tr><td rowspan="2" ><div class="img-wrapper"><img src="images/' + el.photo + '.jpg"></div></td>' +
                            '<td class="bottom name"><a href="products/about/' + el.id + '">' + el.name + '</a></td>' +
                            '<td rowspan="2"><input type="number" min="1" max="' + el.inStock + '" value="'  + el.count + '"></td>' +
                            '<td class="bottom sum-title">сумма</td>' +
                            '<td rowspan="2"> <button onclick="deleteFromChart('+ el.id +')">Видалити</button> </td>' +
                        '</tr>' +
                        '<tr><td class="price">' + el.cost + ' Грн.</td><td class="sum">' + (el.count * el.cost) + ' Грн.</td></tr>' +
                    '</table>'

    const countElement = div.querySelector('input');

    countElement.onchange = function() {
        const newCount = document.querySelector('.product-' + el.id + ' input').value;
        const sumElement = document.querySelector('.product-' + el.id + ' .sum');

        let chart = JSON.parse(getCookie('chart'))
        if (chart[el.id]) {
            if (el.inStock < newCount) return;
            chart[el.id].count = newCount;
            setCookie('chart', JSON.stringify(chart));

            sumElement.innerHTML = newCount * el.cost + ' Грн.';

            updateMainSum();
        }
    }
    return div;
}

function getChart(id) {
    const el = document.getElementById(id);

    if(getCookie('chart')){
        let chart = JSON.parse(getCookie('chart'));
        chart.map((elem) => {if(elem) el.append(createChartElement(elem))})

    el.append(createMainSumDiv());

    } else
        el.innerHTML = 'Обрати товари ви можете <a href="/products">тут</a>';
}

function createMainSumDiv() {
    const div = document.createElement('div');
    div.classList.add('text-center');
    div.innerHTML = '<span>Сума замовлення - </span><span class="main-sum">0 Грн.</span>\n' +
        '    <button onclick="submitChart(\'chart\')">Підтвердити замовлення</button>\n';

    return div;
}

function deleteFromChart(id) {

    let chart = JSON.parse(getCookie('chart'));
    delete chart[id];

    setCookie('chart', JSON.stringify(chart));

    const productElement = document.querySelector('.product-' + id);
    productElement.remove();

    showCountInChart();
    updateMainSum();
}

function updateMainSum() {
    let sum = 0;
    const sumContainer = document.querySelector('.main-sum');

    if(getCookie('chart')) {
        let chart = JSON.parse(getCookie('chart'));

        chart.map((el) => {
            if (el) sum += el.count * el.cost
        });
    }
        sumContainer.innerHTML = sum + ' Грн.'

}

function submitChart(id){
    if(getCookie('chart')) {
        const el = document.getElementById(id);

        el.innerHTML = 'Дякуэмо за ваше замовлення!!!!';
        deleteCookie('chart');
        updateMainSum();
    }
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value) {
    let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + '; max-age=3600*24*31; path=/';

    document.cookie = updatedCookie;
}

function deleteCookie(name) {
    setCookie(name, "", {
        'max-age': -1
    })
}

showCountInChart()