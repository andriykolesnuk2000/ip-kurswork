<?php
function autorize($login, $password){
    include 'application/php/pdo.php';

    $stmt = $pdo->prepare('SELECT * FROM users WHERE login = :login AND password = :password');
    $stmt->execute(array('login' => $login, 'password' => $password));

    if ($row = $stmt->fetch())
    {
        $_SESSION['isAuthorized'] = 'true';
        $_SESSION['login'] = $row['login'];
        $_SESSION['name'] = $row['name'];

        //echo $row['name'] . "\n";
        return true;

    }
    else return false;
}