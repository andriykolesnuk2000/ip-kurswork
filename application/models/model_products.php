<?php
function get_data_from_sql($type = null)
{
    include 'application/php/pdo.php';

    $output = array();
    $count = 0;


    $separator = ' WHERE ';

    $sql = 'SELECT * FROM products';

    if($type != null) {
        $_GET['type'][] = $type;
    }

    if(isset($_GET['type'])){
        foreach ($_GET['type'] as $type){
            $sql .= $separator . ' category = \'' . $type . '\' ';
            $separator = ' OR ';
        }
        $separator = ' AND ';
    }

    if(isset($_GET['min-price'])){
        $sql .= $separator . ' cost > \'' . $_GET['min-price'] . '\' ';
        $separator = ' AND ';
    }

    if(isset($_GET['max-price'])){
        $sql .= $separator . ' cost < \'' . $_GET['max-price'] . '\' ';
        $separator = ' AND ';
    }

    if(isset($_GET['sort']) && $_GET['sort'] != 'default')
        $sql .= ' order by ' . $_GET['sort'] . '';


    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    while($row = $stmt->fetch()){


        $output[] = $row;
        $count++;
    }

    $minMax = getMinMax();

    $output['count'] = $count;
    $output['min-price'] = $minMax[0];
    $output['max-price'] = $minMax[1];
    $output['page_count'] = round (($count / 6) + 0.5, 0, PHP_ROUND_HALF_DOWN);
    return $output;
}

function getMinMax(){
    include 'application/php/pdo.php';

    $minPrice = 999999;
    $maxPrice = 0;
    $output = array();

    $sql = 'SELECT * FROM products';

    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    while($row = $stmt->fetch()){
        if($row['cost'] > +$maxPrice) $maxPrice = $row['cost'];
        if($row['cost'] < +$minPrice) $minPrice = $row['cost'];
    }

    $output[] = $minPrice;
    $output[] = $maxPrice;

    return $output;
}

class Model_Products extends Model
{
    public function get_data()
    {
        $data = get_data_from_sql();
        $output = array('page_count' => $data['page_count'], 'min-price' => $data['min-price'], 'max-price' => $data['max-price']);


        for($i = 0; $i < 6 && $i < $data['count']; $i++)
            $output[] = $data[$i];

        return $output;
    }

    public function get_data_one_product($id)
    {
        include 'application/php/pdo.php';

        $output = array();

        $sql = 'SELECT * FROM products WHERE id = :id';

        $stmt = $pdo->prepare($sql);
        $stmt->execute(array('id' => $id));
        $output = $stmt->fetch();

        return $output;
    }

    public function get_page_data($number, $type = null)
    {
        $products = get_data_from_sql($type);
        $output = array('page_count' => $products['page_count'], 'page_number' => $number, 'min-price' => $products['min-price'], 'max-price' => $products['max-price']);


        $start = 6*($number - 1);
        for($i = $start; $i < $products['count'] && $i < $start + 6; $i++)
            $output[] = $products[$i];

        return $output;
    }


}