<?php

$this->model = new Model_Products();
$this->view = new View();

$data = $this->model->get_data();
$this->view->generate('products_view.php', 'template_view.php', $data);
?>

<script>
    marcAsActive('main');
    marcAsActive('products');
</script>