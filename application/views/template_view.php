<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title>Главная</title>
    <link type="text/css" rel="stylesheet" href="http://courseproject/css/style.css">
    <link type="text/css" rel="stylesheet" href="http://courseproject/css/footer.css">
    <link type="text/css" rel="stylesheet" href="http://courseproject/bootstrap/css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="http://courseproject/bootstrap/css/bootstrap-grid.css">
    <link type="text/css" rel="stylesheet" href="http://courseproject/bootstrap/css/bootstrap-reboot.css">

    <script type="text/javascript" src="http://courseproject/application/js/main.js"></script>
</head>
<body>
    <div class="content">

        <?php include 'menu_view.php'; ?>
        <?php include 'application/views/'.$content_view; ?>
    </div>
    <footer class="myFooter">
        <div class="container">
            <ul>
                <li><a href="http://courseproject/products">Товари</a></li>
                <li><a href="http://courseproject/chart">Не забудьте оформити замовлення</a></li>
                <li><a href="http://courseproject/authorization">Ви можете авторизуватися</a></li>
                <li><a href="http://courseproject/aboutUser">Або перевірити свою інформацію</a></li>
            </ul>
            <p class="footer-copyright">© 2020 Kolesnik Shop</p>
        </div>
    </footer>
    <script src="http://courseproject/application/js/chart.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="http://courseproject/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="http://courseproject/bootstrap/js/bootstrap.bundle.js"></script>
</body>
</html>
