<?php
if(isset($_SESSION['login']) && $_SESSION['login'] == 'admin')
    echo '<form method="post" enctype="multipart/form-data" action="#" class="col-sm-8 col-md-6 col-lg-4">
        <div class="form-group">
            <label for="name">Ім\'я</label>
            <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp" placeholder="Ім\'я" required>
    
        </div>
        <div class="form-group">
            <label for="cost">Ціна</label>
            <input type="number" class="form-control" id="cost" name="cost" placeholder="Ціна" min="1" required>
        </div>
        <div class="form-group">
            <label for="count">Кількість</label>
            <input type="number" class="form-control" id="count" name="count" placeholder="Кількість" min="1" required>
        </div>
        <div class="form-group">
            <label for="type">Категорія</label>
            <select id="type" name="type" class="form-control">
                <option value="home">Для занять вдома</option>
                <option value="Clothes">Спортивний одяг</option>
            </select>
        </div>
        <div class="form-group">
            <label for="description">Опис</label>
            <textarea name="description" id="description" class="form-control" rows="7" maxlength="500" required></textarea>
        </div>
        <div class="form-group">
            <label>Фото товару в форматі jpg
            <input type="file" name="photo"></label>
        </div>
        <button type="submit" class="btn btn-primary">Додати</button>
        <input type="hidden" name="token" value="' . rand(10000,99999) . '" />
        <p class="error"></p>
    </form>';
else
    echo 'У вас немає доступу ло цієї сторінки';



