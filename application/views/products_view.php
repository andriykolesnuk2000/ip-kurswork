<?php

$routes = explode('/', $_SERVER['REQUEST_URI']);
$lastPart = array_pop($routes);

$lastPart = explode('?', $lastPart);

$home = '';
$clothes = '';
$min = $data['min-price'];
$max = $data['max-price'];
$default = '';
$name = '';
$cost = '';

if(isset($_GET['type']))
{
    foreach ($_GET['type'] as $el)
        if($el == 'home') $home = 'checked';
        if($el == 'Clothes') $clothes = 'checked';
}

if(isset($_GET['min-price']))
{
    $min = $_GET['min-price'];
}

if(isset($_GET['max-price']))
{
    $max = $_GET['max-price'];
}

if(isset($_GET['sort']))
{
   switch ($_GET['sort']){
       case 'default' : $default = 'selected';
        break;
       case 'name' : $name = 'selected';
        break;
       case 'cost' : $cost = 'selected';
   }
}

echo '
<h1 class="text-center">Спорттовари</h1>
<div class="row justify-content-center">
<form action="#" method="get" class="col-lg-2 col-md-3 col-sm-4 col-5 products-filter">
    <select name="sort"  class="sort_type" >
    <option value="default" ' . $default . '>Сортувати за датою</option>
    <option value="name" ' . $name . '>Сортувати за назвою</option>
    <option value="cost" ' . $cost . '>Сортувати за ціною</option>
    </select>
    <label class="mt-2">Мінімальна ціна<br>
    <input type="number" min="' . $data['min-price'] . '" max="' . $data['max-price'] . '" class="min-price-num" onchange="setValue(\'min-price-rng\', event)" value="' . $min . '" name="min-price"></label>
    <input type="range" min="' . $data['min-price'] . '" max="' . $data['max-price'] . '" class="min-price-rng" onchange="setValue(\'min-price-num\', event)" value="' . $min . '">
    
    <label class="mt-2 md">Максимальна ціна<br>
    <input type="number" min="' . $data['min-price'] . '" max="' . $data['max-price'] . '" class="max-price-num" onchange="setValue(\'max-price-rng\', event)" value="' . $max . '" name="max-price"></label>
    <input type="range" min="' . $data['min-price'] . '" max="' . $data['max-price'] . '" class="max-price-rng" onchange="setValue(\'max-price-num\', event)" value="' . $max . '">
    
    <div class="mt-2">
        Типи товарів:
        <label><input type="checkbox" value="home" name="type[]"' . $home .'> Для занять вдома</label>
        <label><input type="checkbox" value="Clothes" name="type[]"' . $clothes .'> Спортивний одяг</label>
    </div>
    
    <input type="submit" value="Оновити">
</form>
<div class="products col-lg-8 col-md-7 col-sm-6">';
foreach($data as $product)
{
    if(isset($product['name']))
    echo'<div class="product col-lg-4 col-md-6 col-sm-12 text-center mt-2"><div class="img-wrapper justify-content-center text-center"><img src="http://courseproject/images/'.$product['photoName'].
        '.jpg"></div>
<div class="product-name name-wrapper">'
        .$product['name'].' </div><div class="product-desc desc-wrapper"> '.$product['description'].
        ' </div><div class="product-cost cost-wrapper"> <strong>'.$product['cost'].' Грн.</strong></div>
        <p>'. checkOnAvailability($product). '</p>
        <p><a href="/products/about/'. $product['id'] .'"><button>Детальніше</button></a></p>
</div>';

}


echo '</div></div>
<div class="pages">';
for($i = 1; $i <= $data['page_count']; $i++)
    echo '<a href="http://courseproject/products' . $data['type_product'] . '/page/' . $i . getLastPart($lastPart) . '"><div class="page-' . $i . '">' . $i . '</div></a>';

echo'</div><script>
marcAsActive("products")
</script>';

if(isset($data['page_number'])){
    echo '<script>
marcActiveProductPage('. $data['page_number'] . ')
</script>';
} else
    echo '<script>
marcActiveProductPage(1)
</script>';

function checkOnAvailability($product){
    if($product['count'] != 0)
        return '<button onclick="addToChart('.$product['id'].',\''.$product['name'].'\',\''.$product['photoName'] .'\', ' . $product['cost'] . ', ' . $product['count'] . ')">Додати в козину</button>';
    else return 'Нема в наявності';
}

function getLastPart($lastPart)
{
    if(!empty($lastPart[1]))
        return '?' . $lastPart[1];
    else return '';
}