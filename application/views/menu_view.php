<nav class="navbar navbar-expand-sm navbar-light bg-light">
    <a class="navbar-brand" href="/">Курсовий</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item main"><a class="nav-link" href="/">Головна</a></li>
<!--            <li class="nav-item "><a class="nav-link" href="/portfolio">Портфолио</a></li>-->
<!--            <li class="nav-item"><a class="nav-link" href="/contacts">Контакти</a></li>-->
<!--            <li class="nav-item"><a class="nav-link" href="/services">Сервіси</a></li>-->
            <li class="nav-item products dropdown"><a class="nav-link dropdown-toggle" href="/products">Товари</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/products">Всі</a>
                    <a class="dropdown-item" href="/products/forHome">Для дому</a>
                    <a class="dropdown-item" href="/products/Clothes">Спортивний одяг</a>
                </div>
            </li>



            <li class="nav-item chart"><a class="nav-link" href="/chart">Корзина(<span class="count-in-chart">0</span>)</a></li>
                <!--            <li class="nav-item active">-->
<!--                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>-->
<!--            </li>-->
<!--            <li class="nav-item">-->
<!--                <a class="nav-link" href="#">Link</a>-->
<!--            </li>-->
<!--            <li class="nav-item dropdown">-->
<!--                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--                    Dropdown-->
<!--                </a>-->
<!--                <div class="dropdown-menu" aria-labelledby="navbarDropdown">-->
<!--                    <a class="dropdown-item" href="#">Action</a>-->
<!--                    <a class="dropdown-item" href="#">Another action</a>-->
<!--                    <div class="dropdown-divider"></div>-->
<!--                    <a class="dropdown-item" href="#">Something else here</a>-->
<!--                </div>-->
<!--            </li>-->
<!--            <li class="nav-item">-->
<!--                <a class="nav-link disabled" href="#">Disabled</a>-->
<!--            </li>-->
            <?php
            if(isset($_SESSION['login']) && $_SESSION['login'] == 'admin')
                echo '<li class="nav-item products dropdown"><a class="nav-link dropdown-toggle" href="#">Адміністрування</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/insert">Додавання</a>
                    <a class="dropdown-item" href="/delete">Видалення</a>
                </div>
            </li>';
            ?>
        </ul>
        <?php
        if(isset($_SESSION['name']))
            echo '<a href="/aboutUser">' . $_SESSION['name'] . '</a>';
        else
            echo '<a href="/authorization">Авторизація</a>
                <span>   </span>
              <a href="/registration">Реєстрація</a>';
        ?>
    </div>
</nav>

