<?php

if (isset($_SESSION['login']) && $_SESSION['login'] == 'admin')
    echo '<form method="post" action="#" class="col-sm-8 col-md-6 col-lg-4">
        <div class="form-group">
            <label for="id">ID продукту</label>
            <input type="text" class="form-control" id="id" name="id" placeholder="Id" required>
        </div>
    
        
        <button type="submit" class="btn btn-primary">Видалити</button>
        <input type="hidden" name="token" value="' . rand(1, 99999) . '" />
        <p class="error"></p>
    </form>';
else
    echo 'У вас немає доступу ло цієї сторінки';