<?php

class Controller_Authorization extends Controller
{

    function action_index()
    {
        if(isset($_SESSION['name']))
            header('Location: /aboutUser', true);

        if(isset($_POST['login']) && isset($_POST['password'])){
            include 'application/php/auth.php';
            if(autorize($_POST['login'], $_POST['password'])) {
                header('Location: /', true);
                exit();
            }
            else{
                $this->view->generate('authorization_view.php', 'template_view.php');
                echo '<script>printError("error", "<br>Логін або пароль вказані невірно")</script>';
                return;
            }
        }

        $this->view->generate('authorization_view.php', 'template_view.php');
    }
}

