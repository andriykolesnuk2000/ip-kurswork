<?php
class Controller_Products extends Controller
{
    function __construct()
    {
        $this->model = new Model_Products();
        $this->view = new View();
    }

    function action_index()
    {

        $data = $this->model->get_data();
        $this->view->generate('products_view.php', 'template_view.php', $data);
    }

    function action_about($id)
    {
        $data = $this->model->get_data_one_product($id);
        $this->view->generate('one_product_view.php', 'template_view.php', $data);
    }

    function action_page($number)
    {
        $data = $this->model->get_page_data($number);
        $this->view->generate('products_view.php', 'template_view.php', $data);
    }

    function action_forHome($page = null)
    {
        if($page != null)
        {
            $routes = explode('/', $page);
            if(!empty($routes[1]))
            {
                $data = $this->model->get_page_data($routes[1], 'home');
                $data['type_product'] = '/forHome';
                $this->view->generate('products_view.php', 'template_view.php', $data);
                return;
            }
        }

        $data = $this->model->get_page_data(1, 'home');
        $data['type_product'] = '/forHome';
        $this->view->generate('products_view.php', 'template_view.php', $data);
    }

    function action_Clothes($page = null)
    {
        if($page != null)
        {
            $routes = explode('/', $page);
            if(!empty($routes[1]))
            {
                $data = $this->model->get_page_data($routes[1], 'Clothes');
                $data['type_product'] = '/Clothes';
                $this->view->generate('products_view.php', 'template_view.php', $data);
                return;
            }
        }

        $data = $this->model->get_page_data(1, 'Clothes');
        $data['type_product'] = '/Clothes';
        $this->view->generate('products_view.php', 'template_view.php', $data);
    }
}
