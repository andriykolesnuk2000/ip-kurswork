<?php
class Controller_Main extends Controller
{
    function __construct()
    {
        include "application/models/model_products.php";
        $this->model = new Model_Products();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->get_data();
        $this->view->generate('main_view.php', 'empty_view.php', $data);
    }
}
