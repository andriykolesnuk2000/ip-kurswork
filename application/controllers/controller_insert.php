<?php
class Controller_Insert extends Controller
{
     function action_index()
    {
        if(isset($_POST['name'])) {
            if (isset($_SESSION['lastToken']) && $_POST['token'] == $_SESSION['lastToken'])
            {
                $this->view->generate('insert_view.php', 'template_view.php');
                return;
            }
            else
            {
                $_SESSION['lastToken'] = $_POST['token'];

                $result = $this->addNewProduct();
                if($result === true) {
                    $this->view->generate('insert_view.php', 'template_view.php');
                    echo '<script>alertMessage("Товар ' . $_POST['name'] . ' успішно додано")</script>';
                }
                else{
                    $this->view->generate('insert_view.php', 'template_view.php');
                    echo '<script>printError("error", "' . $result . '")</script>';
                }
                return;
            }
        }

        $this->view->generate('insert_view.php', 'template_view.php');
    }

    public function addNewProduct(){
        if(!(isset($_POST['name']) && isset($_POST['cost']) && isset($_POST['count']) && isset($_POST['type']) && isset($_POST['description'])))
             return 'Не передано всі необхідні параметри';

        include 'application/php/pdo.php';

        $name = $_POST['name'];
        $cost = $_POST['cost'];
        $count = $_POST['count'];
        $type = $_POST['type'];
        $description = $_POST['description'];

        if(isset($_FILES['photo']) && $_FILES['photo']['name'] != '') {
            if($this->can_upload($_FILES['photo']) === true) {
                copy($_FILES['photo']['tmp_name'], 'images/' . $_FILES['photo']['name']);
                $photoName = explode('.', $_FILES['photo']['name'])[0];
            } else return 'Помилка при завантаженні файлу';
        } else $photoName = 'default';
        try {
            $stmt = $pdo->prepare('INSERT INTO products (`name`, `description`, `cost`, `category`, `count`, `photoName`) VALUES (:name, :description, :cost, :category, :count, :photoName)');
            $stmt->execute(array('name' => $name, 'description' => $description, 'cost' => $cost, 'category' => $type, 'count' => $count, 'photoName' => $photoName));
            return true;
        }
        catch(Exception $e) {return 'Помилка при додаванні';}
    }

    function can_upload($file){
        if($file['name'] == '')
            return 'Ви не выбрали файл.';

        if($file['size'] == 0)
            return 'Файл слишком большой.';

        $getMime = explode('.', $file['name']);
        $mime = strtolower(end($getMime));
        $types = array('jpg');

        if(!in_array($mime, $types))
            return 'Недопустимый тип файла.';

        return true;
    }
}