<?php
class Controller_Delete extends Controller
{
    function action_index()
    {
        if(isset($_POST['id'])) {
            if (isset($_SESSION['lastToken']) && $_POST['token'] == $_SESSION['lastToken'])
            {
                $this->view->generate('delete_view.php', 'template_view.php');
                return;
            }
            else
            {
                $_SESSION['lastToken'] = $_POST['token'];

                if($this->isProductAre($_POST['id'])){

                    if($this->delete($_POST['id'])){
                        $this->view->generate('delete_view.php', 'template_view.php');
                        echo '<script>alertMessage("Товар з ід ' . $_POST['id'] . ' успішно видалено")</script>';
                        return;
                    } else {
                        $this->view->generate('delete_view.php', 'template_view.php');
                        echo '<script>printError("error", "Помилка при видаленні")</script>';
                        return;
                    }

                } else {

                    $this->view->generate('delete_view.php', 'template_view.php');
                    echo '<script>printError("error", "Товар з таким ід не знайдено")</script>';
                    return;
                }
            }
        }

        $this->view->generate('delete_view.php', 'template_view.php');
    }

    function isProductAre($id){
        include 'application/php/pdo.php';

        $stmt = $pdo->prepare('SELECT * FROM products WHERE id = :id');
        $stmt->execute(array('id' => $id));
        if ($row = $stmt->fetch())
        {
            return true;
        }

        return false;
    }

    function delete($id){
        include 'application/php/pdo.php';

        $stmt = $pdo->prepare('DELETE FROM products WHERE id = :id');
        try {
            $stmt->execute(array('id' => $id));
        }
        catch (Exception $e){
            return false;
        }

        return true;
    }
}