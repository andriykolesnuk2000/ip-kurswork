<?php
class Controller_AboutUser extends Controller
{

    function action_index()
    {
        if(isset($_SESSION['name'])) {
            $this->view->generate('aboutUser_view.php', 'template_view.php');
        } else
            header('Location: /authorization', true);
    }

    function action_exit()
    {
        session_destroy();
        header('Location: /',true);
    }

    function action_delete()
    {
        include 'application/php/pdo.php';

        $stmt = $pdo->prepare('DELETE FROM `users` WHERE login = :login');
        $stmt->execute(array('login' => $_SESSION['login']));

        session_destroy();
        header('Location: /',true);

    }

    function action_changeName()
    {
        if(isset($_POST['newName']) && isset($_SESSION['name']) && $_POST['newName'] != $_SESSION['name']){
            include 'application/php/pdo.php';

            $stmt = $pdo->prepare('UPDATE `users` SET name = :newName WHERE login = :login');
            $stmt->execute(array('newName' => $_POST['newName'] ,'login' => $_SESSION['login']));

            $_SESSION['name'] = $_POST['newName'];
            header('Location: /aboutUser', true);
        }

        if(isset($_SESSION['name'])) {
            $this->view->generate('changeName_view.php', 'template_view.php');
        } else
            header('Location: /authorization', true);
    }

    function action_changePassword()
    {
        if(isset($_POST['newPassword']) && isset($_SESSION['login']) && isset($_POST['password'])){
            include 'application/php/pdo.php';

            $stmt = $pdo->prepare('SELECT password FROM users WHERE login = :login');
            $stmt->execute(array('login' => $_SESSION['login']));
            $row = $stmt->fetch();
            if ($row['password'] != $_POST['password'])
            {
                $this->view->generate('changePassword_view.php', 'template_view.php');
                echo '<script>printError("error", "<br>Ви ввели невірний пароль")</script>';
                return;
            }

            $stmt = $pdo->prepare('UPDATE `users` SET password = :newPassword WHERE login = :login');
            $stmt->execute(array('newPassword' => $_POST['newPassword'] ,'login' => $_SESSION['login']));

            header('Location: /aboutUser', true);
        }

        if(isset($_SESSION['name'])) {
            $this->view->generate('changePassword_view.php', 'template_view.php');
        } else
            header('Location: /authorization', true);
    }
}