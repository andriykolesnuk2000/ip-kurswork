<?php
class Controller_Registration extends Controller
{
    function action_index()
    {
        if(isset($_POST['login']) && isset($_POST['pass']) && isset($_POST['rePass']) && isset($_POST['name']))
            if(!reg($_POST['login'], $_POST['pass'], $_POST['rePass'], $_POST['name']))
            {
                $this->view->generate('registration_view.php', 'template_view.php');
                echo '<script>printError("error", "<br>Користувач з таким логіном вже зареєстрований")</script>';
                return;
            }
        $this->view->generate('registration_view.php', 'template_view.php');
    }
}

function reg($login, $password, $rePassword, $name){
    include 'application/php/pdo.php';
    include 'application/php/auth.php';

    $stmt = $pdo->prepare('SELECT * FROM users WHERE login = :login');
    $stmt->execute(array('login' => $_POST['login']));
    if ($row = $stmt->fetch())
    {
        //echo 'Користувач з таким логіном вже зареєстрований <br>';
        return false;
    }

    $query = 'INSERT INTO `users` (`login`, `password`, `name`) VALUE ( :login, :password, :name )';
    $stmt = $pdo->prepare($query);
    $stmt->execute(array('login' => $login, 'password' => $password, 'name' => $name));


    autorize($login, $password);
    header('Location: /aboutUser', true);
    exit();
};